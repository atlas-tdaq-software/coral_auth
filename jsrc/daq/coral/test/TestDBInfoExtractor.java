package daq.coral.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import daq.coral.DBInfoExtractor;
import daq.coral.DBInfoExtractor.BadInfoException;


public class TestDBInfoExtractor {
    private String logicalName;
    private String serviceName;

    @Before
    public void setUp() throws Exception {
        this.logicalName = "LOG_MANAGER";
        this.serviceName = "oracle://ATONR_ADG/ATLAS_LOG_MESSG";
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCreateFromLogicalName() {
        System.out.println("---> Test using the DB logical name...");

        try {
            final DBInfoExtractor ie = DBInfoExtractor.createFromLogicalName(this.logicalName);
            System.out.println("User name: " + ie.getUser());
            System.out.println("Password: " + ie.getPassword());
            System.out.println("Connection string: " + ie.getConnectionString());
        }
        catch(final BadInfoException e) {
            e.printStackTrace();
            Assert.fail("Failed to create info extractor using logical name");
        }
    }

    @Test
    public void testCreateFromServiceName() {
        System.out.println("---> Test using the DB service name...");

        try {
            final DBInfoExtractor ie = DBInfoExtractor.createFromServiceName(this.serviceName);
            System.out.println("User name: " + ie.getUser());
            System.out.println("Password: " + ie.getPassword());
            System.out.println("Connection string: " + ie.getConnectionString());
        }
        catch(final BadInfoException e) {
            e.printStackTrace();
            Assert.fail("Failed to create info extractor using service name");
        }
    }
}
