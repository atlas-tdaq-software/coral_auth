package daq.coral;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.AbstractMap.SimpleEntry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 * This is an utility class to extract information about a DB connection using the standard CORAL setup. In order for the class to properly
 * work, the following environment variables have to be defined and valid:
 * <ul>
 * <li><b>CORAL_DBLOOKUP_PATH</b>: this variable should point to a path (or multiple paths) containing the CORAL DB lookup file, whose name
 * is <i>dblookup.xml</i>;</li>
 * <li><b>CORAL_AUTH_PATH</b>: this variable should point to a path (or multiple paths) containing the CORAL DB authentication file, whose
 * name is <i>authentication.xml</i>;</li>
 * <li><b>TNS_ADMIN</b>: this variable should point to the path containing the <i>tnsnames.ora</i> file.</li>
 * </ul>
 * This class also properly sets the value of the <i>oracle.net.tns_admin</i> property so that the JDBC Thin driver can locate the
 * <i>tnsnames.ora</i> file.
 */

public class DBInfoExtractor {
    private final static String LOOKUP_ENV_VAR = "CORAL_DBLOOKUP_PATH";
    private final static String AUTH_ENV_VAR = "CORAL_AUTH_PATH";
    private final static String TNS_ENV_VAR = "TNS_ADMIN";

    private final static String DEFAULT_AUTH_PATH = System.getenv(DBInfoExtractor.AUTH_ENV_VAR);
    private final static String DEFAULT_AUTH_FILE_NAME = "authentication.xml";
    private final static String DEFAULT_DB_LOOKUP_PATH = System.getenv(DBInfoExtractor.LOOKUP_ENV_VAR);
    private final static String DEFAULT_DB_LOOKUP_FILE_NAME = "dblookup.xml";
    private final static String TNS_ORA_FILE = System.getenv(DBInfoExtractor.TNS_ENV_VAR);

    /**
     * This can be the DB logical name or the DB service name
     */
    private final String dbIdentifier;

    /**
     * The name of the user to be used for connecting to the DB
     */
    private final String user;

    /**
     * The user's password to be used for connecting to the DB
     */
    private final String paswd;

    /**
     * The DB's connection string
     */
    private final String connectString;

    /**
     * Exception thrown in case of failures extracting any DB information.
     */
    static public class BadInfoException extends Exception {
        private static final long serialVersionUID = 1114674584894392138L;

        BadInfoException(final String message, final Throwable cause) {
            super(message, cause);
        }

        BadInfoException(final String message) {
            super(message);
        }
    }

    static {
        // This is mandatory
        System.setProperty("oracle.net.tns_admin", DBInfoExtractor.TNS_ORA_FILE);
    }

    /**
     * Constructor.
     * 
     * @param serviceName The DB service name (e.g., <i>oracle://ATONR_ADG/ATLAS_LOG_MESSG</i>)
     * @param dbAuthFilePath Path to the CORAL authentication file
     * @throws BadInfoException Failure in extracting DB's information
     */
    private DBInfoExtractor(final String serviceName, final String dbAuthFilePath) throws BadInfoException {
        this.dbIdentifier = serviceName;

        final SimpleEntry<String, String> userInfo = this.getUserInfo(serviceName, dbAuthFilePath);
        this.user = userInfo.getKey();
        this.paswd = userInfo.getValue();

        this.connectString = this.buildConnectionString(serviceName);
    }

    /**
     * Constructor.
     * 
     * @param logicalName The DB logical name (e.g., <i>LOG_MANAGER</i>)
     * @param dbLookupFilePath Path Path to the CORAL lookup file
     * @param dbAuthFilePath Path to the CORAL authentication file
     * @throws BadInfoException Failure in extracting DB's information
     */
    private DBInfoExtractor(final String logicalName, final String dbLookupFilePath, final String dbAuthFilePath) throws BadInfoException {
        this.dbIdentifier = logicalName;

        final String serviceName = this.getServiceName(logicalName, dbLookupFilePath);

        final SimpleEntry<String, String> userInfo = this.getUserInfo(serviceName, dbAuthFilePath);
        this.user = userInfo.getKey();
        this.paswd = userInfo.getValue();

        this.connectString = this.buildConnectionString(serviceName);
    }

    /**
     * Factory to create a {@link DBInfoExtractor} instance from the DB's logical name
     * 
     * @param logicalName The DB logical name (e.g., <i>LOG_MANAGER</i>)
     * @return A valid instance of {@link DBInfoExtractor}
     * @throws BadInfoException Failure in extracting DB's information
     * @throws IllegalArgumentException <i>logicalName</i> is null or empty
     */
    public static DBInfoExtractor createFromLogicalName(final String logicalName) throws BadInfoException {
        if((logicalName == null) || (logicalName.isEmpty() == true)) {
            final String msg = "The DB logical name cannot be null or empty";
            throw new IllegalArgumentException(msg);
        }

        if((DBInfoExtractor.DEFAULT_DB_LOOKUP_PATH == null) || (DBInfoExtractor.DEFAULT_DB_LOOKUP_PATH.isEmpty() == true)) {
            final String msg = "The DB lookup file path cannot be invalid or empty, please check the value of the \""
                               + DBInfoExtractor.LOOKUP_ENV_VAR + "\" env variable (current value is \""
                               + DBInfoExtractor.DEFAULT_DB_LOOKUP_PATH + "\")";
            throw new BadInfoException(msg);
        }

        if((DBInfoExtractor.DEFAULT_AUTH_PATH == null) || (DBInfoExtractor.DEFAULT_AUTH_PATH.isEmpty() == true)) {
            final String msg = "The DB authentication file path cannot be invalid or empty, please check the value of the \""
                               + DBInfoExtractor.AUTH_ENV_VAR + "\" env variable (current value is \"" + DBInfoExtractor.DEFAULT_AUTH_PATH
                               + "\")";
            throw new BadInfoException(msg);
        }

        return new DBInfoExtractor(logicalName, DBInfoExtractor.DEFAULT_DB_LOOKUP_PATH, DBInfoExtractor.DEFAULT_AUTH_PATH);
    }

    /**
     * Factory to create a {@link DBInfoExtractor} instance from the DB's service name
     * 
     * @param serviceName The DB service name (e.g., <i>oracle://ATONR_ADG/ATLAS_LOG_MESSG</i>)
     * @return A valid instance of {@link DBInfoExtractor}
     * @throws BadInfoException Failure in extracting DB's information
     * @throws IllegalArgumentException <i>serviceName</i> is null or empty
     */
    public static DBInfoExtractor createFromServiceName(final String serviceName) throws BadInfoException {
        if((serviceName == null) || (serviceName.isEmpty() == true)) {
            final String msg = "The DB logical name cannot be null or empty";
            throw new IllegalArgumentException(msg);
        }

        if((DBInfoExtractor.DEFAULT_AUTH_PATH == null) || (DBInfoExtractor.DEFAULT_AUTH_PATH.isEmpty() == true)) {
            final String msg = "The DB authentication file path cannot be invalid or empty, please check the value of the \""
                               + DBInfoExtractor.AUTH_ENV_VAR + "\" env variable (current value is \"" + DBInfoExtractor.DEFAULT_AUTH_PATH
                               + "\")";
            throw new BadInfoException(msg);
        }

        return new DBInfoExtractor(serviceName, DBInfoExtractor.DEFAULT_AUTH_PATH);
    }

    /**
     * It returns the name of the user to be used for connecting to the DB
     * 
     * @return The name of the user to be used for connecting to the DB
     */
    public String getUser() {
        return this.user;
    }

    /**
     * It returns the user password to be used for connecting to the DB
     * 
     * @return The user password to be used for connecting to the DB
     */
    public String getPassword() {
        return this.paswd;
    }

    /**
     * It returns the DB connection string (in the JDBC Thin driver format)
     * 
     * @return The DB connection string (in the JDBC Thin driver format)
     */
    public String getConnectionString() {
        return this.connectString;
    }

    /**
     * It builds the connection string
     * 
     * @param serviceName The DB service name (e.g., <i>oracle://ATONR_ADG/ATLAS_LOG_MESSG</i>)
     * @return The DB connection string (in the JDBC Thin driver format)
     * @throws BadInfoException Failure in building the connection string
     */
    private String buildConnectionString(final String serviceName) throws BadInfoException {
        try {
            final URI serviceURI = new URI(serviceName);
            final String connStr = "jdbc:oracle:thin:@" + serviceURI.getRawAuthority();
            return connStr;
        }
        catch(final URISyntaxException ex) {
            throw new BadInfoException("Cannot build the database connection string: " + ex.getMessage(), ex);
        }
    }

    /**
     * It extracts the user name and the corresponding password to be used to access the DB
     * 
     * @param serviceName The DB service name (e.g., <i>oracle://ATONR_ADG/ATLAS_LOG_MESSG</i>)
     * @param dbAuthFilePath Path to the CORAL authentication file
     * @return A pair of strings, with the user name as key and the password as value
     * @throws BadInfoException Some error occurred extracting the needed information
     */
    private SimpleEntry<String, String> getUserInfo(final String serviceName, final String dbAuthFilePath) throws BadInfoException {
        final String[] paths = dbAuthFilePath.split(":");

        SimpleEntry<String, String> userInfo = null;

        for(int i = 0; i < paths.length; ++i) {
            try {
                final DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();

                // If the path is relative, then refer it to the working directory
                Path p = Paths.get(paths[i], DBInfoExtractor.DEFAULT_AUTH_FILE_NAME);
                if(p.isAbsolute() == false) {
                    p = Paths.get("").toAbsolutePath().resolve(p);
                }

                final Document doc = db.parse(p.toFile());

                final XPath xpath = XPathFactory.newInstance().newXPath();
                final String userName =
                                      xpath.evaluate("/connectionlist/connection[translate(@name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='"
                                                     + serviceName.toLowerCase() + "']/parameter[@name='user']/@value", doc);
                final String pwd =
                                 xpath.evaluate("/connectionlist/connection[translate(@name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='"
                                                + serviceName.toLowerCase() + "']/parameter[@name='password']/@value", doc);

                if((userName.isEmpty() == true) || (pwd.isEmpty() == true)) {
                    if(i != (paths.length - 1)) {
                        ers.Logger.log("Cannot retrieve credentials for the DB connection in directory \"" + paths[i]
                                           + "\": user name and/or password are not found");
                        ers.Logger.log("Trying with the next file in the authorization path (" + dbAuthFilePath + ")");
                    } else {
                        final String errMsg =
                                            "Cannot retrieve credentials for the DB connection (user name and/or password are not found). "
                                              + "Check that the service name of the connection (" + this.dbIdentifier
                                              + "), the paths to the authentication (" + DBInfoExtractor.DEFAULT_AUTH_PATH
                                              + ") and the the lookup files (" + DBInfoExtractor.DEFAULT_DB_LOOKUP_PATH + ") "
                                              + "are correct and consistent.";
                        throw new BadInfoException(errMsg);
                    }
                } else {
                    userInfo = new SimpleEntry<String, String>(userName, pwd);
                    break;
                }
            }
            catch(final XPathExpressionException | ParserConfigurationException | SAXException | IOException ex) {
                if(i != (paths.length - 1)) {
                    ers.Logger.log("Cannot retrieve credentials for the DB connection in directory \"" + paths[i] + "\": " + ex);
                    ers.Logger.log("Trying with the next file in the authorization path (" + dbAuthFilePath + ")");
                } else {
                    throw new BadInfoException("Cannot retrieve credentials for the DB connection: " + ex.getMessage(), ex);
                }
            }
        }

        return userInfo;
    }

    /**
     * It extracts the name of the DB service starting from its logical name
     * 
     * @param logicalName The DB logical name (e.g., <i>LOG_MANAGER</i>)
     * @param dbLookupFilePath Path to the CORAL authentication file
     * @return The name of the DB service
     * @throws BadInfoException Some error occurred extracting the needed information
     */
    private String getServiceName(final String logicalName, final String dbLookupFilePath) throws BadInfoException {
        final String[] paths = dbLookupFilePath.split(":");

        String serviceName = null;

        for(int i = 0; i < paths.length; ++i) {
            try {
                final DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();

                // If the path is relative, then refer it to the working directory
                Path p = Paths.get(paths[i], DBInfoExtractor.DEFAULT_DB_LOOKUP_FILE_NAME);
                if(p.isAbsolute() == false) {
                    p = Paths.get("").toAbsolutePath().resolve(p);
                }

                final Document doc = db.parse(p.toFile());

                final XPath xpath = XPathFactory.newInstance().newXPath();
                final String lookupString =
                                          "/servicelist/logicalservice[translate(@name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='"
                                            + logicalName.toLowerCase()
                                            + "']/service[(@accessMode='read' or @accessMode='update') and @authentication='password']/@name";
                final String foundService = xpath.evaluate(lookupString, doc);

                if(foundService.isEmpty() == true) {
                    if(i != (paths.length - 1)) {
                        ers.Logger.log("Cannot find the DB service name in directory \"" + paths[i] + "\"");
                        ers.Logger.log("Trying with the next file in the lookup path (" + dbLookupFilePath + ")");
                    } else {
                        final String errMsg = "Cannot retrieve credentials for the DB connection (the service name is not found). "
                                              + "Check that the logical name of the connection (" + this.dbIdentifier
                                              + "), the paths to the authentication (" + DBInfoExtractor.DEFAULT_AUTH_PATH
                                              + ") and the the lookup files (" + DBInfoExtractor.DEFAULT_DB_LOOKUP_PATH + ") "
                                              + "are correct and consistent.";
                        throw new BadInfoException(errMsg);
                    }
                } else {
                    serviceName = foundService;
                }
            }
            catch(final XPathExpressionException | ParserConfigurationException | SAXException | IOException ex) {
                if(i != (paths.length - 1)) {
                    ers.Logger.log("Cannot retrieve the DB service name in directory \"" + paths[i] + "\": " + ex);
                    ers.Logger.log("Trying with the next file in the lookup path (" + dbLookupFilePath + ")");
                } else {
                    throw new BadInfoException("Cannot retrieve the service name for the DB connection: " + ex.getMessage(), ex);
                }
            }
        }

        return serviceName;
    }

}
