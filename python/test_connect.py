#!/usr/bin/env tdaq_python

import cx_Oracle
import coral_auth
import sys
import os

def test(*connect_params):
    try:
        if len(connect_params) == 1:
            connection = cx_Oracle.connect(connect_params[0])
        else:
            connection = cx_Oracle.connect(*connect_params)
    except LookupError as err:
        print(err)
        sys.exit(1)

    cursor = connection.cursor()
    result = cursor.execute("select * from partition_ids where tdaq_release = 'tdaq-06-01-01'")
    r = cursor.fetchall()
    print(r[0])

# Check that the areas in these env variables are accessible to us
# If not then skip the check: this might be a local build outside CERN
# or in a container image with no DB access.
for var in [ 'CORAL_DBLOOKUP_PATH', 'CORAL_AUTH_PATH' ]:
  if not os.access(os.environ.get(var, '/xxx'), os.R_OK, effective_ids=True):
    sys.exit(66)

print('Testing: get_connection_string...')
test(coral_auth.get_connection_string("LOG_MANAGER"))

print('Testing: get_connection_parameters...')
test(*coral_auth.get_connection_parameters("LOG_MANAGER"))

print('Testing: get_connection_parameters_from_connection_string...')
test(*coral_auth.get_connection_parameters_from_connection_string(
       "oracle://ATONR_ADG/ATLAS_LOG_MESSG"))
