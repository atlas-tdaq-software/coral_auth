""" coral_auth module

"""

__author__ = "Andrei Kazarov"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date: 2016/11/15 00:00:00 $"

import os
import sys
import xml.etree.ElementTree as et


def get_connection_parameters_from_connection_string(coral_connect_string):
    """
    Lookup CORAL_AUTH_PATH for authentication.xml file containing credentials
    matching the given CORAL connection string. Return connection parameters in
    the form a tuple: user, password, service_name

    Usage example:
      conn_params = get_connection_parameters_from_connection_string('oracle://DBNAME/DBSCHEMA')
      orcl = cx_Oracle.connect(*conn_params)
    """
    try:
        dirs = os.environ['CORAL_AUTH_PATH'].split(':')
    except KeyError:
        raise LookupError('CORAL_AUTH_PATH must be defined')

    found = 0
    for dir in dirs:
        file = dir + "/authentication.xml"
        if os.path.exists(file):
            found = 1
            tree = et.parse(file)
            conn = tree.findall("./connection[@name='" + coral_connect_string + "']")
            if len(conn) > 0:
                user = conn[0].findall("./parameter[@name='user']")[0].attrib['value']
                pwd = conn[0].findall("./parameter[@name='password']")[0].attrib['value']
                break

    if found == 0:
        raise LookupError('authentication.xml not found in CORAL_AUTH_PATH')

    if len(conn) == 0:
        raise LookupError('Service for {} not found in CORAL_AUTH_PATH'.format(coral_connect_string))

    parts = coral_connect_string.split('/')
    service_name = parts[2]
    return user, pwd, service_name


def get_connection_parameters(logical_name):
    try:
        dirs = os.environ['CORAL_DBLOOKUP_PATH'].split(':')
    except KeyError:
        raise LookupError('CORAL_DBLOOKUP_PATH must be defined')

    found = 0
    for dir in dirs:
        file = dir + "/dblookup.xml"
        if os.path.exists(file):
            found = 1
            tree = et.parse(file)
            logserv = tree.findall("./logicalservice[@name='" + logical_name + "']")
            if len(logserv) > 0:
                coral_connect = logserv[0].findall("./service")[0].attrib['name']
                parts = coral_connect.split('/')
                service_name = parts[2]
                break
    if found == 0:
        raise LookupError('dblookup.xml not found in CORAL_DBLOOKUP_PATH')

    if len(logserv) == 0:
        raise LookupError('Service name {} not found in CORAL_DBLOOKUP_PATH'.format(logical_name))

    return get_connection_parameters_from_connection_string(coral_connect)


def get_connection_string(logical_name):
    """ returns Oracle connection string for a Coral logical name

    Example of use (to access Log Manager tables:

    import cx_Oracle
    import coral_auth

    try:
      connection = cx_Oracle.connect(coral_auth.get_connection_string("LOG_MANAGER"))
    except LookupError as err:
      print(err)
      sys.exit(1)
    """
    user, pwd, service_name = get_connection_parameters(logical_name)
    return user + '/' + pwd + '@' + service_name
