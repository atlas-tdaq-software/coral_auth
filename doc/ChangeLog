#--------------------------------------------------------------------------
# File and Version Information:
#  $Id$
#
# Description:
#  ChangeLog file for package coral_auth
#------------------------------------------------------------------------

Package author: Andrei Salnikov

Please describe any modifications that you made to the package in the
reverse time order.

Tag: coral_auth-01-17-01
2011-03-02 Andy Salnikov
- migrate to CORAL_2_3_14, header file AuthenticationCredentials.h was
  moved to different directory

Tag: coral_auth-01-17-00
2010-11-04 Andy Salnikov
- cleaned requirements from unneeded dependencies
- tested CoralAuth by running get_credentials against itself, so I'm 
  confident that my changes did not break anything critical

Tag: andy-20101104-04
2010-11-04 Andy Salnikov
- removed doubly-nested coral_auth namespace

Tag: andy-20101104-03
2010-11-04 Andy Salnikov
- moved things in src/coral_auth and coral_auth/coral_auth one level up
- modified requirements file for new package layout

Tag: andy-20101104-02
2010-11-04 Andy Salnikov
- moved pfzlib/Exec to coca
- removed coral_auth/pfzlib and src/pfzlib

Tag: andy-20101104-01
2010-11-04 Andy Salnikov
- DAL stuff has moved to coca
- removing pfzlib/System and pfzlib/FileUtils classes
- coral_auth is free from pfzlib dependencies now
- will move pfzlib/Exec to coca package, have to commit before that

Tag: coral_auth-01-16-01
2010-10-20 Andy Salnikov
- removed pfzlib/Threads

Tag: coral_auth-01-15-00
2010-09-21 Andy Salnikov
- got rid of the pfzlib/Fatal, ers handles uncaught exceptions already 
  and abort() can be called directly

Tag: coral_auth-01-14-00
2010-09-21 Andy Salnikov
- removed database wrapper code and signal handling class:
    D      coral_auth/pfzlib/SQL.h
    D      coral_auth/pfzlib/Signals.h
    D      coral_auth/pfzlib/basic.h
    D      coral_auth/pfzlib/DB.h
    D      src/pfzlib/SQL.cxx
    D      src/pfzlib/Signals.cxx
    D      src/pfzlib/DB.cxx
- database stuff was re-implemented and simplified in the client code
  (mda and coca) using pure CORAL
- signal handling was also simplified on the client side with native
  UNIX calls

Tag: coral_auth-01-13-01
2010-08-25 Andy Salnikov
- adding -lclocks to shared libraries flags

Tag: coral_auth-01-13-00
2010-08-23 Andy Salnikov
- re-tag with coral_auth-01-13-00 for nightly

Tag: andy-20100823-01
2010-08-23 Andy Salnikov
- lot's of changes, intermediate commit
- still few pfzlib classes left here, most complex stuff - DB, Threads,
  System, etc. - used in all three packages. Will deal with those 
  in future commits
- removed stuff:
    pfzlib/BaseIssues
    pfzlib/Conversions  - use boost::lexical_cast
    pfzlib/Exclusive    
    pfzlib/InitLater    - use standard auto_ptr
    pfzlib/MessRep      - use ers
    pfzlib/Named        - useless base class replaced with direct code
    pfzlib/namespace.h
    pfzlib/NextIdTypes
    pfzlib/Size
    pfzlib/Strings      - use boost and standard stuff
    pfzlib/Time         - use clocks/Time instead
    pfzlib/Timer        - not useful, was used in one place only
    pfzlib/Units
- moved to different locations:
    pfzlib/ErrorCodes    - moved to coral_auth/exit_codes
    pfzlib/NextId        - moved to coca/common
    pfzlib/Predicate     - moved to mda/archive
    pfzlib/RunParameters - moved to mda/archive
    pfzlib/Singleton     - moved to coral_auth/exit_codes
    pfzlib/Str2Id        - moved to coca/common
    pfzlib/test/run_params.cxx - moved to mda/test
- lot's of changes everywhere related to disappearing stuff
- coca and mda were tested to work with all these changes in a simple 
  test partition
- tagging with private tag, will re-tag later for nightly

Tag: coral_auth-01-12-04
2010-07-29 Andy Salnikov
- Further cleanup:
  - removed pfzlib/Environment
  - removed pfzlib/WrapIPC
  - removed pfzlib/WrapDAL
  - replaced above three with simple code
  - removed pfzlib/utils/aggregate.sh

Tag: coral_auth-01-12-03
2010-07-28 Andy Salnikov
- Further cleanup:
  - removed unused pfzlib/Apply2Pointers.h header
  - removed pfzlib/CORAL_headers.h, replace include with CORAL includes
  - removed unused pfzlib/Aggregate.h
  - removed class pfzlib::Exclusive::Any

Tag: coral_auth-01-12-02
2010-07-27 Andy Salnikov
- removed FunStack tools
- removed all FS_MARK macros everywhere
- should be able to debug now

Tag: coral_auth-01-12-01
2010-07-27 Andy Salnikov
- removed several files which contain trivial code and either not used 
  or easily replaced with straight code:
D      coral_auth/pfzlib/RegExp.h
D      coral_auth/pfzlib/Traceable.h
D      coral_auth/pfzlib/Container.h
D      coral_auth/pfzlib/Malloc.h
D      src/pfzlib/RegExp.cxx
D      src/pfzlib/Malloc.cxx
- few changes needed in other places due to the removed stuff 
- external API did not change at all

Tag: coral_auth-01-12-00
2010-07-21 Andy Salnikov
- start cleanup of the dependencies, working on simplest things for now
  which do not break existing client code
- removed wrapper includes from coral_auth/pfzlib/no_warnings and 
  complete directory, clients switch to original non-wrapped code
- moved WrapROOT class to mda package, this is the only known client
  of that class
- package becomes ROOT-free and cmdl-free
- updated requirements:
  - removed ROOT libs
  - removed headers copy for no_warnings
  - removed generation of revisions.txt file
- added doc/ChangeLog
