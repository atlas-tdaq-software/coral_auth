// $Id$
// $Name:  $

#include "coral_auth/exit_codes/GetCredentialsCodes.h"
#include "ers/ers.h"

#include <RelationalAccess/IAuthenticationService.h>
#include <RelationalAccess/IAuthenticationCredentials.h>
#include <RelationalAccess/AuthenticationCredentials.h>
#include <RelationalAccess/AuthenticationServiceException.h>
#include <CoralKernel/Context.h>

#include <ostream>
#include <cstdlib>
#include <boost/lexical_cast.hpp>

#include <unistd.h>

using namespace daq::coral_auth;

namespace {

    ERS_DECLARE_ISSUE( errors, CoralException, "Generic CORAL exception", )

    ERS_DECLARE_ISSUE( errors, UnhandledException,
                "Unhandled '" << name << "' exception was thrown",
                ((const char *)name) )

    ERS_DECLARE_ISSUE ( errors, SysCallError, 
     "system call `" << syscall << "' failed with error: " << ::strerror (errno), 
     ((std::string) syscall)) 

}

int
main_actual (int argc, const char * const *argv)
{
    // cmd-line arguments
    ERS_ASSERT(argc == 3);
    const std::string conn_str (argv[1]);
    const int output_fd = boost::lexical_cast<int> (argv[2]);

    // read credentials
    coral::Context& context = coral::Context::instance();
    context.loadComponent("CORAL/Services/XMLAuthenticationService");
    
    coral::IHandle<coral::IAuthenticationService> xml_as = context.query<coral::IAuthenticationService> () ;

    if (!xml_as.isValid()) {
    	//std::cout << "Ciao" << std::endl;
	return 1;
    }
    
    const coral::IAuthenticationCredentials &creds = xml_as->credentials (conn_str);
    
    // write credentials out
    std::string out = creds.valueForItem (creds.userItem()) + " "
        + creds.valueForItem (creds.passwordItem()) + "\n";
    size_t len = out.size();
    ssize_t tmp = write (output_fd, out.c_str(), len);
    if (tmp == -1 || size_t(tmp) != len) {
        throw ::errors::SysCallError (ERS_HERE, "write");
    }
    return 0;
}

int
main (int argc, char **argv)
{
    GetCredentialsCodes::List codes;
    try {
        int ret_code = main_actual (argc, argv);
        if (ret_code == 0) {
            ERS_DEBUG (1, "get_credentials process exiting successfully");
        } else {
            ERS_DEBUG (1, "get_credentials process exiting with error: " << ret_code);
        }  
        return ret_code;
    }
    catch (const coral::AuthenticationServiceException &ex) {
        ers::error (errors::CoralException (ERS_HERE, ex.what()));
        return codes().GetCodes().XML_Authentication;
    }
    catch (const ::errors::SysCallError &ex) {
        ers::error (ex);
        return codes().GetCodes().Write;
    } catch (const std::exception &ex) {
        ers::error(::errors::UnhandledException(ERS_HERE, "standard", ex));
        return codes().GetCodes().UncaughtSTD;
    } catch (...) {
        ers::fatal(::errors::UnhandledException(ERS_HERE, "unknown"));
        return codes().GetCodes().UncaughtUnknown;
    }
}

