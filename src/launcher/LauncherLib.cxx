// $Id$
// $Name$

#include "coral_auth/launcher/LauncherLib.h"

#include <fstream>
#include <cassert>
#include <pwd.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

using namespace daq::coral_auth;

/*
 * RegExp
 */

LauncherLib::RegExp::RegExp (const std::string &Pattern)
    // throw (Exception)
{
    pattern_str = Pattern;
    // re_syntax_options: global variable defined in regex.h
    __typeof__ (::re_syntax_options) keep = re_syntax_options;
    re_syntax_options = RE_SYNTAX_EGREP;

    pattern.translate = NULL;
    pattern.fastmap = new char[256];
    pattern.buffer = NULL;
    pattern.allocated = 0;
    const char *error;
    error = re_compile_pattern (Pattern.c_str(), Pattern.size(), &pattern);
    if (error) {
        delete[] pattern.fastmap;
        THROW ("invalid regular expression: \"" << Pattern << "\"; error: "
               << error);
    }

    // re_syntax_options: global variable defined in regex.h
    re_syntax_options = keep;
}

LauncherLib::RegExp::RegExp (const RegExp &Source)
{
    pattern_str = Source.GetPattern();
    // re_syntax_options: global variable defined in regex.h
    __typeof__ (::re_syntax_options) keep = re_syntax_options;
    re_syntax_options = RE_SYNTAX_EGREP;

    pattern.translate = NULL;
    pattern.fastmap = new char[256];
    pattern.buffer = NULL;
    pattern.allocated = 0;
    const char *error;
    error = re_compile_pattern (pattern_str.c_str(), pattern_str.size(),
                                &pattern);
    if (error) {
        delete[] pattern.fastmap;
        THROW ("invalid regular expression: \"" << pattern_str << "\"; error: "
               << error);
    }

    // re_syntax_options: global variable defined in regex.h
    re_syntax_options = keep;
}


LauncherLib::RegExp::~RegExp ()
{
    delete[] pattern.fastmap;
}

bool
LauncherLib::RegExp::Match (const std::string &String) const
    throw ()
{
    int tmp = re_search (&pattern, String.c_str(), String.size(), 0,
                         String.size(), NULL);
    assert (tmp > -2);
    return (tmp >= 0);
}

/*
 * Privileges
 */

const bool LauncherLib::Privileges::ON = true;
const bool LauncherLib::Privileges::OFF = false;

LauncherLib::Privileges::Privileges ()
    throw ()
{
    real_id_start = getuid();
    eff_id_start = geteuid();
    fixed = false;
    Save();
}

LauncherLib::Privileges::~Privileges ()
{
}

void
LauncherLib::Privileges::Set (bool on)
{
    if (IsSetuid() && ! fixed) {
        uid_t id = SelectUID (on);
        int tmp = seteuid (id);
        if (tmp != 0) {
            THROW ("cannot set uid to: " << id);;
        }
        MR_DEBUG (0, "privileges turned " << (on ? "ON" : "OFF"));
    }
}

void
LauncherLib::Privileges::Save ()
    throw ()
{
    is_on_saved = IsON();
}

void
LauncherLib::Privileges::Restore ()
    throw ()
{
    Set (is_on_saved);
}

void
LauncherLib::Privileges::Fix (bool on)
{
    Set (on);
    if (IsSetuid() && ! fixed) {
        uid_t id = SelectUID (on);
        int tmp1 = setreuid (id, id);
        if (tmp1 != 0) {
            THROW ("setreuid failed");;
        }
        MR_DEBUG (0, "privileges fixed " << (on ? "ON" : "OFF"));
    }
    fixed = true;
}

/*
 * GetHomeDir
 */

std::string
LauncherLib::GetHomeDir ()
{
    struct passwd data;
    struct passwd *check = NULL;
    const size_t bufsize = 4096;
    char buf[bufsize];
    getpwuid_r (geteuid(), &data, buf, bufsize, &check);
    if (check != &data) {
        THROW ("unexpected error (ERANGE) from getpwuid_r");
    }
    return data.pw_dir;
}

/*
 * AuthEntry
 */

LauncherLib::AuthEntry::AuthEntry (const std::string &Parent,
                                   const std::string &Binary,
                                   const std::string &Argument)
    // throw (Exception)
    : parent (Parent), binary (Binary), argument (Argument)
{
}
    
LauncherLib::AuthEntry::~AuthEntry ()
{
}
    
bool
LauncherLib::AuthEntry::Match (const std::string &Parent,
                               const std::string &Binary,
                               const std::string &Argument) const
    throw ()
{
    bool auth = parent.Match (Parent) && binary.Match (Binary)
        && argument.Match (Argument);
    if (auth) {
        MR_DEBUG (1, "request for (" << Parent << " " << Binary << " "
                  << Argument << ") satisfied by entry: "
                  << parent.GetPattern() << " " << binary.GetPattern() << " "
                  << argument.GetPattern());
    }
    return auth;
}

/*
 * AuthFile
 */

LauncherLib::AuthFile::AuthFile (const std::string &Path, Privileges &privs)
{
    privs.Save();
    privs.Set (privs.ON);
    std::ifstream auth_file (Path.c_str());
    privs.Restore();
    if (! auth_file) {
        THROW ("cannot open authorization file: " << Path);
    }
    do {
        std::string line;
        std::getline (auth_file, line);
        // skip empty lines or comments
        bool keep_line = false;
        size_t len = line.size();
        for (size_t k = 0; k < len; ++ k) {
            if (isspace (line[k])) {
                continue;
            }
            if (line[k] != '#') {
                keep_line = true;
            }
            break;
        }
        if (! keep_line) {
            continue;
        }
        // separate into three strings
        std::string parent;
        std::string binary;
        std::string argument;
        std::istringstream buf (line);
        buf >> parent >> binary >> argument;

        MR_DEBUG (3, line << " <= 1 => " << buf.good() << " " << buf.eof()
                  << " " << buf.fail() << " " << buf.bad());

        if (buf.fail()) {
            MR_ERROR ("3 strings required: " << line);
            continue;
        }
        // only three strings
        std::string tmp;
        buf >> tmp;

        MR_DEBUG (3, line << " <= 2 => " << buf.good() << " " << buf.eof()
                  << " " << buf.fail() << " " << buf.bad());

        if (! buf.fail()) {
            MR_ERROR ("only 3 strings required: " << line);

            continue;
        }
        // ok, line is good
        std::string parent_w ("^" + parent + "$");
        std::string binary_w ("^" + binary + "$");
        std::string argument_w ("^" + argument + "$");
        entries.push_back (AuthEntry (parent_w, binary_w, argument_w));
    } while (auth_file.good());
}

LauncherLib::AuthFile::~AuthFile ()
{
}

bool
LauncherLib::AuthFile::Match (const std::string &Parent,
                              const std::string &Binary,
                              const std::string &Argument) const
    throw ()
{
    for (std::list<AuthEntry>::const_iterator it = entries.begin();
         it != entries.end(); ++ it)
    {
        if (it->Match (Parent, Binary, Argument)) {
            return true;
        }
    }
    MR_DEBUG (1, "request for (" << Parent << " " << Binary << " "
              << Argument << ") failed");
    return false;
}

/*
 * GetParent
 */

std::string
LauncherLib::GetParent (Privileges &privs)
{
    std::ostringstream link_buf;
    link_buf << "/proc/" << getppid() << "/exe";
    std::string link = link_buf.str();
    
    // usual problems: argument is size_t, but return value is ssize_t,
    // and we don't want to copy templated Conversions::abs here
    //const size_t bufsize = 4096;
#define BUFSIZE 4096

    char buf[BUFSIZE];
    privs.Save();
    privs.Set (privs.OFF);
    ssize_t tmp = readlink (link.c_str(), buf, BUFSIZE);
    privs.Restore();
    if (tmp == -1) {
        THROW ("unexpectd error from readlink(" << link << "): "
               << strerror (errno));
    }
    assert (tmp > 0);
    if (tmp >= BUFSIZE) {
        THROW ("unexpectd length for link content: " << tmp);
    }
    buf[tmp] = 0;
    return buf;
}
    
/*
 * SetEnv
 */

void
LauncherLib::SetEnv  (const std::string &Variable,
                      const std::string &Value)
{
    int tmp = setenv (Variable.c_str(), Value.c_str(), 1);
    if (tmp != 0) {
        THROW ("unexpectd error from setenv(" << Variable << "): "
               << strerror (errno));
    }
}

