// $Id$
// $Name$

#include "coral_auth/launcher/LauncherLib.h"
#include "coral_auth/launcher/LauncherBits.h"

#include <errno.h>
#include <string.h>
#include <unistd.h>

using namespace daq::coral_auth;

#define AUTH_FILE "coral_auth_launcher_auth.txt"

int
usage ()
{
    MR_ERROR ("Usage: " << program_invocation_short_name
              << "[--no-privs] [--env <var1> <value1>"
              " [--env <var2> <value2> [...]]] <prog> <arg>");
    return LauncherBits::HelpRequested;
}

int
main_actual (int argc, char **argv)
{
    LauncherLib::Privileges privs;
    privs.Set (LauncherLib::Privileges::OFF);

    bool need_privileges = true;
    char **cmdline = NULL;
    std::string binary;
    std::string argument;

    for (int k = 1; k < argc; ++ k) {
        std::string arg (argv[k]);
        MR_DEBUG (3, "arg: " << arg);
        if (arg == "-h" || arg == "--help") {
            return usage();
        } else if (arg == "--no-privs") {
            need_privileges = false;
            privs.Fix (LauncherLib::Privileges::OFF);
        } else if (arg == "--env") {
            if (k + 2 >= argc) {
                return usage();
            }
            LauncherLib::SetEnv (argv[k + 1], argv[k + 2]);
            k += 2;
        } else {
            if (k + 2 > argc) {
                return usage();
            }
            cmdline = argv + k;
            binary = arg;
            argument = argv[k + 1];
            break;
        }
    }

    if (need_privileges && privs.IsSetuid()) {
        privs.Set (LauncherLib::Privileges::ON);
        std::string path = LauncherLib::GetHomeDir() + "/" + AUTH_FILE;
        privs.Set (LauncherLib::Privileges::OFF);
        LauncherLib::AuthFile auth_file (path, privs);
        std::string parent (LauncherLib::GetParent(privs));
        if (! auth_file.Match (parent, binary, argument)) {
            MR_ERROR ("exec '" << binary << "' '" << argument << "':"
                      " authorization denied");
            return LauncherBits::AuthorizationDenied;
        }
        privs.Fix (LauncherLib::Privileges::ON);
    } else {
        if (privs.IsON()) {
            THROW ("unexpected privileges");
        }
    }

    execvp (binary.c_str(), cmdline);
    // Got here? Error!!
    MR_ERROR ("exec '" << binary << "' '" << argument << "': failed");
    return LauncherBits::ExecError;
}

int
main (int argc, char **argv)
{
    try {
        int ret_code = main_actual (argc, argv);
        if (ret_code == 0) {
            MR_DEBUG (0, "process exiting successfully");
        } else {
            MR_DEBUG (0, "process exiting with error: " << ret_code);
        }  
        return ret_code;
    }
    catch (const std::exception &ex) {
        MR_ERROR (ex.what());
        return LauncherBits::UncaughtSTD;
    }
    catch (...) {
        MR_ERROR ("unknown exception caught");
        return LauncherBits::UncaughtUnknown;
    }
}

