// $Id$
// $Name:  $

#include "coral_auth/coral_auth/CoralAuth.h"
#include "coral_auth/exit_codes/GetCredentialsCodes.h"
#include "coral_auth/exit_codes/LauncherCodes.h"
#include "ers/ers.h"

#include <memory>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

using namespace daq::coral_auth;
using namespace daq::CoralAuth;

namespace {

    ERS_DECLARE_ISSUE( errors, ConnStrError,
                       "unknown connection string: " << connstr ,
                       ((std::string) connstr) )

    ERS_DECLARE_ISSUE ( errors, ErrnoError, 
       "call to `" << syscall << "' failed with error: " << ::strerror (errno), 
       ((std::string) syscall)) 

    ERS_DECLARE_ISSUE(errors, CoralAuthError, error, ((std::string) error))

    ERS_DECLARE_ISSUE (errors, ForkError, "cannot create new process", )
}


namespace daq
{

  namespace CoralAuth
  {
    const std::string authFileName = "authentication.xml";
  }
}

const std::string daq::CoralAuth::XMLAuthenticationService::component("IAuthenticationService::credentials");

XMLAuthenticationService::XMLAuthenticationService( const std::string& componentName )
  : coral::Service( componentName ), creds (this->name())
{
}

XMLAuthenticationService::~XMLAuthenticationService()
{

}


const coral::IAuthenticationCredentials & 
XMLAuthenticationService::Found(const std::string &ConnectionString) const
{
    const int_cred_t &int_cred = found[ConnectionString];
    creds.registerItem (creds.userItem(), int_cred.first);
    creds.registerItem (creds.passwordItem(), int_cred.second);
    return creds;
}

std::string 
XMLAuthenticationService::clean_LD_LIBRARY_PATH () const
{
    if (const char* ld = getenv ("LD_LIBRARY_PATH")) {
        std::vector<std::string> tokens;
        boost::split (tokens, ld, boost::is_any_of(":"), boost::token_compress_on);
        std::string sp_new;
        for (std::vector<std::string>::const_iterator it = tokens.begin();
             it != tokens.end(); ++ it)
        {
            // skip components like "coral_auth", "coral_auth/*", "*/coral_auth" or "*/coral_auth/*"
            std::string ext_comp = '/' + *it + '/';
            if ( ext_comp.find("/coral_auth/") != std::string::npos ) {
                ERS_DEBUG (1, "skipping component of LD_LIBRARY_PATH: " << *it);
            } else {
                sp_new += *it + ":";
            }
        }
        return sp_new;
    } else {
        return std::string();
    }
}


const daq::CoralAuth::XMLAuthenticationService::int_cred_t
XMLAuthenticationService::getCredentials(const std::string &ConnectionString, bool setuid) const
{
    // create pipes
    int pipe_fds[2];
    int tmp1 = pipe (pipe_fds);
    if (tmp1 != 0) {
        ::errors::ErrnoError err (ERS_HERE, "pipe");
        ers::error(err);
        throw coral::AuthenticationServiceException
            (err.what(), component, this->name());
    }
    int input_fd = pipe_fds[0];
    int output_fd = pipe_fds[1];

    // execute getCredentials; LD_LIBRARY_PATH has to be cleaned in order 
    // to get the original coral authentication service
    const char* argv[16];  // reserve enough space for all args
    int argc = 0;

    argv[argc++] = "coral_auth-launcher";

    const std::string& newld = clean_LD_LIBRARY_PATH();
    if (getenv("LD_LIBRARY_PATH")) {
        argv[argc++] = "--env";
        argv[argc++] = "LD_LIBRARY_PATH";
        argv[argc++] = newld.c_str();
    }

    if (! setuid) {
        argv[argc++] = "--no-privs";
    }
    argv[argc++] = "coral_auth-get_credentials";
    argv[argc++] = ConnectionString.c_str();

    const std::string fd_str = boost::lexical_cast<std::string>(output_fd);
    argv[argc++] = fd_str.c_str();
    argv[argc++] = 0;

    // spawn the process
    ERS_DEBUG (3, "Going to exec:" << argv[0]);
    pid_t childpid = ::fork ();
    if (childpid == 0) {

        // this is the child
        close (0);
        execvp (argv[0], (char**)argv);

        // exec failed, using ers is dangerous after fork, we'll just
        // print a message and stop, parent will notice we are not there
        ::fprintf(stderr, "pid[%d], ppid[%d]: FATAL: cannot execute: %s; error: %s",
                getpid(), getppid(), argv[0], strerror(errno));
        ::exit(1);
        
    } else if (childpid == -1) {
        
        ::errors::ForkError err(ERS_HERE, ::errors::ErrnoError (ERS_HERE, "fork"));
        ers::error(err);
        throw coral::AuthenticationServiceException(err.what(), 
                component, this->name());
        
    }
    // this is the server; the child was created successfully
    ERS_DEBUG (2, "pid [" << childpid << "]: " << argv[0]);

    
    // close the input side of the pipe, so that read will return as
    // soon as the child closes it as well
    ERS_ASSERT(close (output_fd) == 0);

    // wait for output from child
    fd_set rfds;
    FD_ZERO (&rfds);
    FD_SET (input_fd, &rfds);
    struct timeval tv;
    tv.tv_sec = 5;
    tv.tv_usec = 0;
    ERS_DEBUG (3, "waiting for input from child");
    int tmp2 = select (input_fd + 1, &rfds, NULL, NULL, &tv);
    switch (tmp2) {
    case 0: {
        ::errors::CoralAuthError err (ERS_HERE, 
                "timeout expired while waiting for credentials");
        ers::error(err);
        throw coral::AuthenticationServiceException(err.what(), 
                component, this->name());
    }
    case -1: {
        if (errno == EINTR) {
            ::errors::CoralAuthError err (ERS_HERE, "got a signal");
            ers::error(err);
            throw coral::AuthenticationServiceException
                (err.what(), component, this->name());
        } else {
            ::errors::ErrnoError err (ERS_HERE, "select");
            ers::error(err);
            throw coral::AuthenticationServiceException
                (err.what(), component, this->name());
        }
    }
    }
    ERS_ASSERT (tmp2 == 1);
    ERS_ASSERT (FD_ISSET (input_fd, &rfds));

    // read output from child
    FILE *input = fdopen (input_fd, "r");
    if (input == NULL) {
        ::errors::ErrnoError err (ERS_HERE, "fdopen");
        ers::error(err);
        throw coral::AuthenticationServiceException
            (err.what(), component, this->name());
    }
    // will be equal to ESPIPE
    errno = 0;

    // read complete pipe contents
    ERS_DEBUG (3, "reading input from child");
    std::string res;
    do {
        char buf[128];
        if ( fgets (buf, sizeof buf, input) ) {
            res += buf;
        } else {
            break;
        }
    } while(true);

    // check error condition
    if (ferror(input)) {
        if (errno) {
            ::errors::ErrnoError err (ERS_HERE, "getline");
            ers::error(err);
            throw coral::AuthenticationServiceException
                (err.what(), component, this->name());
        } else {
            res.clear();
            // do not throw now; wait to know the exit code
            ERS_DEBUG (0, "no data from coral_auth-get_credentials");
        }
    }

    // wait for child to quit, get its return code
    int code = 0;
    if (waitpid(childpid, &code, 0) < 0) {
        // wait has failed for some reason
        ::errors::ErrnoError err(ERS_HERE, "waitpid");
        throw coral::AuthenticationServiceException(err.what(), 
                component, this->name());
    }
    
    // process error code
    if (code != 0) {
        LauncherCodes::List l_codes;
        if (code <= l_codes().GetMaxCode()) {
            throw coral::AuthenticationServiceException
                (l_codes() (code), component, this->name());
        } else  {
            GetCredentialsCodes::List gc_codes;
            throw coral::AuthenticationServiceException
                (gc_codes() (code), component, this->name());
        }
    }

    // did we get any data
    if (res.empty()) {
        ::errors::CoralAuthError err
            (ERS_HERE, "no data from coral_auth_getCredentials");
        ers::error (err);
        throw coral::AuthenticationServiceException
            (err.what(), component, this->name());
    }

    // parse string (user and password separated by a space)
    const char ws[] = " \t\n";
    std::string user;
    std::string passwd;
    std::string::size_type p = res.find_first_not_of(ws);
    if (p != std::string::npos) {
        std::string::size_type p1 = res.find_first_of(ws, p);
        if (p1 != std::string::npos) {
            user = res.substr(p, p1-p);

            p = res.find_first_not_of(ws, p1);
            if (p != std::string::npos) {
                p1 = res.find_first_of(ws, p);
                if (p1 != std::string::npos) {
                    passwd = res.substr(p, p1-p);
                } else {
                    passwd = res.substr(p);                    
                }   
            }
        }
    }
    
    if (user.empty() or passwd.empty()) {
        ::errors::CoralAuthError err (ERS_HERE, "bad syntax for user and password");
        ers::error (err);
        throw coral::AuthenticationServiceException (err.what(), 
                component, this->name());
    }


      //ERS_DEBUG (0, "user: "  << "--" << user << "--");
      //ERS_DEBUG (0, "passwd: "  << "--" << passwd << "--");

    return std::make_pair (user, passwd);
}

const coral::IAuthenticationCredentials &
XMLAuthenticationService::credentials(const std::string &ConnectionString) const
{
    // already failed?
    if (failed.find (ConnectionString) != failed.end()) {
        ::errors::ConnStrError err (ERS_HERE, ConnectionString);
        ers::error (err);
        throw coral::AuthenticationServiceException
            (err.what(), component, this->name());
    }

    // already found?
    if (found.find (ConnectionString) != found.end()) {
        return Found (ConnectionString);
    }

    // put it in "failed"; remove later if ok
    failed.insert (ConnectionString);

    int_cred_t creds;
    try {
        creds = getCredentials (ConnectionString, false);
    }
    catch (const coral::AuthenticationServiceException &ex) {
        ERS_DEBUG (0, ex.what());
        creds = getCredentials (ConnectionString, true);
    }

    // register credentials
    found[ConnectionString] = creds;
    failed.erase (ConnectionString);

    return Found (ConnectionString);
}

const coral::IAuthenticationCredentials &
XMLAuthenticationService::credentials(const std::string &ConnectionString, 
        const std::string &Role) const
{
    if (! Role.empty()) {
        ERS_LOG ("Ignoring role: " << Role);
    }
    return this->credentials (ConnectionString);
}

void 
XMLAuthenticationService::setInputFileName(const std::string& /*inputFileName*/) 
{
   std::cerr << "Change of authentication file name not supported!";
   return;
}
