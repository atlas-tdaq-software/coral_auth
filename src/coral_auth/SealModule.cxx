// $Id$
// $Name$

#include "coral_auth/coral_auth/CoralAuth.h"

#include <SealKernel/ComponentFactory.h>
#include <PluginManager/ModuleDef.h>

using namespace daq::coral_auth;

DEFINE_SEAL_MODULE ();
DEFINE_SEAL_PLUGIN (seal::ComponentFactory, CoralAuth::CoralAuth,
                    CoralAuth::CoralAuth::classContextLabel ());
