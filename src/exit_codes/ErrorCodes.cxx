// $Id$
// $Name$

#include "coral_auth/exit_codes/ErrorCodes.h"
#include "ers/ers.h"

#include <algorithm>

using namespace daq::coral_auth;

/*
 * List
 */

ErrorCodes::List::List ()
    throw ()
{
    min_code = 255;
    max_code = 0;
}

ErrorCodes::List::~List ()
    throw ()
{
}

ErrorCodes::List::List (const std::list<CodeString> &Codes)
    // throw (InvalidCode, RepeatedCode)
{
    min_code = 255;
    max_code = 0;
    for (std::list<CodeString>::const_iterator it = Codes.begin();
         it != Codes.end(); ++ it)
    {
        Add (*it);
    }
}

void
ErrorCodes::List::Add (const CodeString &Code)
    // throw (InvalidCode, RepeatedCode)
{
    if (Code < 1 || Code > 255) {
        throw InvalidCode (ERS_HERE, Code);
    }
    map_it_t it = pool.find (Code);
    if (it != pool.end()) {
        throw RepeatedCode (ERS_HERE, Code);
    }
    pool[(int) Code] = (std::string) Code;
    min_code = std::min (min_code, (int) Code);
    max_code = std::max (max_code, (int) Code);
    ERS_DEBUG (1, "inserted: " << (int) Code
              << "([" << min_code << "," << max_code << "])");
}

std::string
ErrorCodes::List::operator() (int Code) const
    // throw (UnknownCode)
{
    if (Code == 0) {
        return "success";
    }
    map_it_t it = pool.find (Code);
    if (it == pool.end()) {
        throw UnknownCode (ERS_HERE, Code);
    }
    return it->second;
}

