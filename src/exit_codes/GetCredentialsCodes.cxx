// $Id$
// $Name$

#include "coral_auth/exit_codes/GetCredentialsCodes.h"

#include <stdlib.h>

#include "ers/ers.h"

using namespace daq::coral_auth;

/*
 * Codes
 */

GetCredentialsCodes::Codes::Codes (int MinCode)
    : Write (MinCode + 0, "\"write\" syscall failed"),
      UncaughtSTD (MinCode + 1, "uncaught standard exception"),
      UncaughtUnknown (MinCode + 2, "uncaught unknown exception"),
      XML_Seal (MinCode + 3, "cannot load XML authentication service"),
      XML_Authentication (MinCode + 4, "connection string unknown to"
                          " XML authentication service")
{
}

/*
 * List4Singleton
 */

GetCredentialsCodes::List4Singleton::List4Singleton ()
    throw ()
    : codes (l_codes().GetMaxCode() + 1)
{
    ERS_DEBUG (2, "max code = " << l_codes().GetMaxCode());

    try {
        Add (codes.Write);
        Add (codes.UncaughtSTD);
        Add (codes.UncaughtUnknown);
        Add (codes.XML_Seal);
        Add (codes.XML_Authentication);
    }
    catch (const ErrorCodes::RepeatedCode &ex) {
        ers::fatal(ex);
        ::abort();
    }
}
