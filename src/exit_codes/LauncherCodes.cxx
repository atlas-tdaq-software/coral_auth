// $Id$
// $Name$

#include "coral_auth/exit_codes/LauncherCodes.h"

#include <stdlib.h>

#include "coral_auth/launcher/LauncherBits.h"
#include "ers/ers.h"

using namespace daq::coral_auth;

LauncherCodes::List4Singleton::List4Singleton ()
    throw ()
{
    try {
        Add (ErrorCodes::CodeString (LauncherBits::AuthorizationDenied,
                                     "authorization denied"));
        Add (ErrorCodes::CodeString (LauncherBits::ExecError,
                                     "failed to execute binary"));
        Add (ErrorCodes::CodeString (LauncherBits::UncaughtSTD,
                                     "uncaught std::exception"));
        Add (ErrorCodes::CodeString (LauncherBits::UncaughtUnknown,
                                     "uncaught unknown exception"));
        Add (ErrorCodes::CodeString (LauncherBits::HelpRequested,
                                     "help requested"));
    }
    catch (const ErrorCodes::RepeatedCode &ex) {
        ers::fatal(ex);
        ::abort();
    }
}
