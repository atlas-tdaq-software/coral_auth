// $Id$
// $Name$

#ifndef CORAL_AUTH_LAUNCHER_CODES
#define CORAL_AUTH_LAUNCHER_CODES

#include "coral_auth/exit_codes/ErrorCodes.h"
#include "coral_auth/exit_codes/Singleton.h"

namespace daq {
    namespace coral_auth {
        namespace LauncherCodes {
          
            class List4Singleton;
            typedef Singleton::Single<List4Singleton> List;
        }
    }
}

class daq::coral_auth::LauncherCodes::List4Singleton
    : public ErrorCodes::List
{
    friend class Singleton::Single<List4Singleton>;
protected:
    List4Singleton ()
        throw ();
    ~List4Singleton ()
        throw ()
        {}
};

#endif // CORAL_AUTH_LAUNCHER_CODES
