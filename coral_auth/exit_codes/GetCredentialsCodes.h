// $Id$
// $Name$

#ifndef CORAL_AUTH_GET_CREDENTIALS_CODES
#define CORAL_AUTH_GET_CREDENTIALS_CODES

#include "coral_auth/exit_codes/ErrorCodes.h"
#include "coral_auth/exit_codes/LauncherCodes.h"

namespace daq {
    namespace coral_auth {
        namespace GetCredentialsCodes {

            class Codes;
            class List4Singleton;
            typedef Singleton::Single<List4Singleton> List;
        }
    }
}

class daq::coral_auth::GetCredentialsCodes::Codes {
    friend class List4Singleton;
public:
    const ErrorCodes::CodeString Write;
    const ErrorCodes::CodeString UncaughtSTD;
    const ErrorCodes::CodeString UncaughtUnknown;
    const ErrorCodes::CodeString XML_Seal;
    const ErrorCodes::CodeString XML_Authentication;
protected:
    Codes (int MinCode);
};

class daq::coral_auth::GetCredentialsCodes::List4Singleton
    : public ErrorCodes::List
{
    friend class Singleton::Single<List4Singleton>;
private:
    LauncherCodes::List l_codes;
    Codes codes;
protected:
    List4Singleton ()
        throw ();
    ~List4Singleton ()
        throw ()
        {}
public:
    const Codes &GetCodes () const
        {return codes;}
};

#endif // CORAL_AUTH_GET_CREDENTIALS_CODES

