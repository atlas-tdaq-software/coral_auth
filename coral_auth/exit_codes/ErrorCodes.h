// $Id$
// $Name$

#ifndef CORAL_AUTH_ERROR_CODES
#define CORAL_AUTH_ERROR_CODES

#include "ers/ers.h"

namespace daq {
    namespace coral_auth {
    
        ERS_DECLARE_ISSUE (ErrorCodes, Exception, , ) 

        ERS_DECLARE_ISSUE_BASE (ErrorCodes, InvalidCode, Exception,
                                "code not in the range [1,255]: " << code,
                                , ((int) code)) 

        ERS_DECLARE_ISSUE_BASE (ErrorCodes, RepeatedCode, Exception,
                                "repeated error code: " << code,
                                , ((int) code)) 

        ERS_DECLARE_ISSUE_BASE (ErrorCodes, UnknownCode, Exception,
                                "unknown error code: " << code,
                                , ((int) code)) 

        namespace ErrorCodes {
            class CodeString;
            class List;
        }
    }
}

/*
 * CodeString
 */

class daq::coral_auth::ErrorCodes::CodeString
    : public std::pair< int, std::string>
{
public:
    CodeString (int Code, const std::string &String)
        throw ();
    operator int () const
        {return first;}
    operator std::string () const
        {return second;}
};

inline
daq::coral_auth::ErrorCodes::CodeString::
CodeString (int Code, const std::string &String)
    throw ()
    : std::pair< int, std::string> (Code, String)
{
}

/*
 * List
 */

class daq::coral_auth::ErrorCodes::List {
private:
    std::map< int, std::string> pool;
    typedef std::map< int, std::string>::const_iterator map_it_t;
    int min_code;
    int max_code;
protected:
    List ()
        throw ();
    void Add (const CodeString &Code); // throw (InvalidCode, RepeatedCode)
public:
    List (const std::list<CodeString> &Codes); // throw (InvalidCode, RepeatedCode)
    ~List ()
        throw ();
    std::string operator() (int Code) const; //  throw (UnknownCode)
    int GetMinCode () const
        {return min_code;}
    int GetMaxCode () const
        {return max_code;}
};

#endif // CORAL_AUTH_ERROR_CODES
