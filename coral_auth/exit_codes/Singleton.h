// $Id$
// $Name$

#ifndef CORAL_AUTH_SINGLETON
#define CORAL_AUTH_SINGLETON

#include <cassert>

namespace daq {
    namespace coral_auth {
        namespace Singleton {
            template <class Type> class Single;
            template <class Type> class OneForever;
        }
    }
}

/*
 * Single
 */

template <class Type>
class daq::coral_auth::Singleton::Single {
private:
    static Type *ptr;
    static unsigned int count;
public:
    Single ()
        throw ();
    ~Single ()
        throw ();
    Type &Get () const
        {return *ptr;}
    operator Type & () const
        {return *ptr;}
    Type &operator() () const
        {return *ptr;}
};

template <class Type> Type *
daq::coral_auth::Singleton::Single<Type>::ptr = NULL;
template <class Type> unsigned int
daq::coral_auth::Singleton::Single<Type>::count = 0;

template <class Type>
daq::coral_auth::Singleton::Single<Type>::Single ()
    throw ()
{
    if (count == 0) {
        assert (ptr == NULL);
        ptr = new Type;
    } else {
        assert (ptr != NULL);
    }
    ++ count;
}

template <class Type>
daq::coral_auth::Singleton::Single<Type>::~Single ()
    throw ()
{
    assert (ptr != NULL);
    -- count;
    if (count == 0) {
        delete ptr;
        ptr = NULL;
    }
}

/*
 * OneForever
 */

template <class Type>
class daq::coral_auth::Singleton::OneForever {
private:
    static Type *ptr;
public:
    OneForever ()
        throw ();
    ~OneForever ()
        throw ();
    Type &Get () const
        {return *ptr;}
    operator Type & () const
        {return *ptr;}
    Type &operator() () const
        {return *ptr;}
};

template <class Type> Type *
daq::coral_auth::Singleton::OneForever<Type>::ptr = NULL;

template <class Type>
daq::coral_auth::Singleton::OneForever<Type>::OneForever ()
    throw ()
{
    if (ptr == NULL) {
        ptr = new Type;
    }
}

template <class Type>
daq::coral_auth::Singleton::OneForever<Type>::~OneForever ()
    throw ()
{
}

#endif // CORAL_AUTH_SINGLETON
