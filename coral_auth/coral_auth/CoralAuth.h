// $Id$
// $Name:  $
// Author: P.F. Zema
// Modified: 2009/05/25 G. Lehmann Miotto after removal of SEAL from CORAL

#ifndef CORALAUTH_CORALAUTH_H
#define CORALAUTH_CORALAUTH_H

#include <RelationalAccess/IAuthenticationService.h>
#include <RelationalAccess/AuthenticationServiceException.h>
#include <RelationalAccess/AuthenticationCredentials.h>
#include <CoralKernel/Service.h>
#include <CoralKernel/Property.h>
#include "CoralKernel/CoralPluginDef.h"

#include <set>

namespace daq {
  namespace CoralAuth {

 /** 
  * @class XMLAuthenticationService 
  * Overrides of the coral XMLAuthenticationService plugin for more secure passwd handling
  *
  */
  
    class XMLAuthenticationService : public coral::Service,
                                     virtual public coral::IAuthenticationService
    {
    public:
      /// Standard Constructor
      XMLAuthenticationService( const std::string& componentName );

      /// Standard Destructor
      virtual ~XMLAuthenticationService();

    public:
      /**
       * Returns a reference to the credentials object for a given connection string.
       * If the connection string is not known to the service an UnknownConnectionException is thrown.
       */
      const coral::IAuthenticationCredentials& credentials( const std::string& connectionString ) const;

      /**
       * Returns a reference to the credentials object for a given connection string.
       * If the connection string is not known to the service an UnknownConnectionException is thrown.
       * If the role is not known to the service an UnknownRoleException is thrown.
       */
      const coral::IAuthenticationCredentials& credentials( const std::string& connectionString, const std::string& role ) const;

    public:
      /// Sets the input file name
      void setInputFileName(  const std::string& inputFileName );
    private:
    
      static const std::string component;
      // IAuthenticationService declares "credentials" to be cost
      mutable coral::AuthenticationCredentials creds;
      mutable std::set<std::string> failed;
      typedef std::pair<std::string, std::string> int_cred_t;
      mutable std::map<std::string, int_cred_t> found;

      const coral::IAuthenticationCredentials &Found
      (const std::string &ConnectionString) const;

      //std::string & name();
      std::string clean_LD_LIBRARY_PATH () const;
      const int_cred_t getCredentials (const std::string &ConnectionString, bool setuid) const;
   };
}
}
CORAL_PLUGIN_MODULE( "CORAL/Services/XMLAuthenticationService", daq::CoralAuth::XMLAuthenticationService )

#endif
