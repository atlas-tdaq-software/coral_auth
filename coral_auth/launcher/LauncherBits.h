// $Id$
// $Name$

#ifndef CORAL_AUTH_LAUNCHER_BITS
#define CORAL_AUTH_LAUNCHER_BITS

namespace daq {
    namespace coral_auth {
        namespace LauncherBits {
            enum {
                AuthorizationDenied = 1,
                ExecError,
                UncaughtSTD,
                UncaughtUnknown,
                HelpRequested
            };
        }
    }
}

#endif // CORAL_AUTH_LAUNCHER_BITS
