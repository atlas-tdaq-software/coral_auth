// $Id$
// $Name$

#ifndef CORAL_AUTH_LAUNCHER_LIB
#define CORAL_AUTH_LAUNCHER_LIB

#include <iostream>
#include <exception>
#include <list>
#include <string>
#include <sstream>

#include <regex.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

/*
 * Message Reporting macros
 */

#define LOG_OUT(message) do { \
    std::cerr << message << std::endl; \
} while (false)

#ifdef ERS_NO_DEBUG
#define MR_DEBUG( level, message) do {} while (false)
#else
#define MR_DEBUG( level, message) do { \
    if (getenv ("TDAQ_CORAL_AUTH_LAUNCHER_DEBUG")) { \
        LOG_OUT ("DEBUG" << level << " [" << __FUNCTION__ << " at " __FILE__ \
                 ":" << __LINE__ << "]: " << message); \
    } \
} while (false)
#endif

#define MR_ERROR(message) LOG_OUT ("ERROR: " << message)

#define THROW(message) do { \
    std::ostringstream _exception_buf_; \
    _exception_buf_ << "EXCEPTION [" << __FUNCTION__ << " at " __FILE__ ":" \
                << __LINE__ << "]: " << message; \
    throw daq::coral_auth::LauncherLib::Exception \
        (_exception_buf_.str()); \
} while (false)

/*
 * misc classes and functions
 */

namespace daq {
    namespace coral_auth {
        namespace LauncherLib {
            class Exception;
            class RegExp;
            class Privileges;
            std::string GetHomeDir ();
            class AuthEntry;
            class AuthFile;
            std::string GetParent (Privileges &privs);
            void SetEnv (const std::string &Variable,
                         const std::string &Value);
        }
    }
}

/*
 * Exception
 */

class daq::coral_auth::LauncherLib::Exception
    : public std::exception
{
private:
    std::string message;
public:
    Exception (const std::string &Message)
        throw ();
    ~Exception ();
    const char *what () const
        throw ();
};

inline
daq::coral_auth::LauncherLib::Exception::
Exception (const std::string &Message)
    throw ()
    : message (Message)
{
}

inline
daq::coral_auth::LauncherLib::Exception::~Exception ()
    throw ()
{
}

inline
const char *
daq::coral_auth::LauncherLib::Exception::what () const noexcept
{
    return message.c_str();
}

/*
 * RegExp
 */

class daq::coral_auth::LauncherLib::RegExp {
private:
    std::string pattern_str;
    mutable struct ::re_pattern_buffer pattern;
public:
    RegExp (const std::string &Pattern); // throw (Exception)
    RegExp (const RegExp &Source);
    ~RegExp ();
    bool Match (const std::string &String) const
        throw ();
    const std::string &GetPattern () const
        {return pattern_str;}
};

/*
 * Privileges
 */

class daq::coral_auth::LauncherLib::Privileges {
private:
    uid_t real_id_start;
    uid_t eff_id_start;
    uid_t SelectUID (bool on)
        throw ()
        {return on ? eff_id_start : real_id_start;}
    bool fixed;
    bool is_on_saved;
public:
    static const bool ON;
    static const bool OFF;
    Privileges ()
        throw ();
    ~Privileges ();
    uid_t GetRealIdStart () const
        {return real_id_start;}
    uid_t GetEffectiveIdStart () const
        {return eff_id_start;}
    bool IsSetuid () const
        {return real_id_start != eff_id_start;}
    bool IsON () const
        throw ()
        {return IsSetuid() && geteuid() == eff_id_start;}
    void Set (bool on);
    void Save ()
        throw ();
    void Restore ()
        throw ();
    void Fix (bool on);
};
 
/*
 * AuthEntry
 */
        
class daq::coral_auth::LauncherLib::AuthEntry {
private:
    RegExp parent;
    RegExp binary;
    RegExp argument;
public:
    AuthEntry (const std::string &Parent,
               const std::string &Binary,
               const std::string &Argument); 
        // throw (Exception);
    ~AuthEntry ();
    bool Match (const std::string &Parent,
                const std::string &Binary,
                const std::string &Argument) const
        throw ();
};

/*
 * AuthFile
 */

class daq::coral_auth::LauncherLib::AuthFile {
private:
    std::list<AuthEntry> entries;
public:
    AuthFile (const std::string &Path, Privileges &privs);
    ~AuthFile ();
    bool Match (const std::string &Parent,
                const std::string &Binary,
                const std::string &Argument) const
        throw ();
};

#endif // CORAL_AUTH_LAUNCHER_LIB

